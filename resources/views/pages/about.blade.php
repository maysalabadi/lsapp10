@extends('layouts.app')  

 @section('content')         
<h1>{{$title}}</h1> 
<h1>Maysa Labadi</h1>  
<h2>SUMMARY OF QUALIFICATIONS</h2>

<ul class="list-group">
<li class="list-group-item">Local & global full-time professional experiences for more than 15 years in different types of programming and in various sizes of organizations</li>
<li class="list-group-item">Strong interpersonal, communications, analytical and leadership management skills</li>
<li class="list-group-item">Superior knowledge and experience in Web programming, CMS, ORACLE RDB ,SQL, PL/SQL, MySQL/Maria, Forms, reports,triggers, functions </li>
<li class="list-group-item">SQL SERVER and T-SQL</li>
<li class="list-group-item">My goal is to continue to be involved in developing computer applications and systems that are not only Intellectually challenging but also has practical benefit and impact</li>
</ul>

<ul class="list-group">
<li class="list-group-item">5+ years of development experience primarily with PHP </li>
<li class="list-group-item">Front-end development: HTML5, CSS3, Javascript</li>
<li class="list-group-item">Proficient with frameworks preferably in Laravel</li>
<li class="list-group-item">Experience with MySQL and databases design</li>
<li class="list-group-item">Creating modern responsive WordPress websites, frontend and backend </li>
<li class="list-group-item">Testing across different browsers to meet web standards compliance</li>
</ul>
 @endsection
