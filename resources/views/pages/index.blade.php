
 @extends('layouts.app')  

 @section('content')    
<div class="jumbotron text-center">
        <h1>Welcome to My Blog Developed in laravel/framework 6.0</h1>
        <p class="lead">You need to Register to publish your posts in our blog.</p>
        <p><a class="btn btn-primary btn-lg" href="/login" role="button">Login</a>  
        <a class="btn btn-primary btn-lg btn-success" href="/register" role="button">Register</a></p>
      </div>
 @endsection